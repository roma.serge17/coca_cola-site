//https://github.com/VincentGarreau/particles.js/
// Particles Parameters

particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 30,
      "density": {
        "enable": true,
        "value_area": 400
      }
    },
    "color": {
      "value": "#ffffff"
    },
    "shape": {
      "type": "image",
      "stroke": {
        "width": 3,
        "color": "#fff"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "./img/litle_drop.png",
        "width": 10,
        "height": 10
      }
    },
    "opacity": {
      "value": 0.7,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 5,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 20,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": false,
      "distance": 10,
      "color": "#ffffff",
      "opacity": 0.6,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 5,
      "direction": "bottom",
      "random": true,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": true,
        "rotateX": 300,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": true,
        "mode": "bubble"
      },
      "onclick": {
        "enable": false,
        "mode": "repulse"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 5,
        // "line_linked": {
        //   "opacity": 1
        // }
      },
      "bubble": {
        "distance": 5,
        "size": 5,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      // "repulse": {
      //   "distance": 200,
      //   "duration": 0.2
      // },
      // "push": {
      //   "particles_nb": 4
      // },
      // "remove": {
      //   "particles_nb": 2
      // }
    }
  },
  "retina_detect": true
});

// Nav Hamburger

let toggled = false;
const nav = document.getElementsByClassName('nav_burger')[0];
const btn = document.getElementsByClassName('nav-tgl')[0];
btn.onclick = function (evt) {
  if (!toggled) {
    toggled = true;
    evt.target.classList.add('toggled');
    nav.classList.add('active');
  } else {
    toggled = false;
    evt.target.classList.remove('toggled');
    nav.classList.remove('active');
  }
}


// TAB

const tabsTitle = document.querySelectorAll(".tab-head li");
for (let i = 0; i < tabsTitle.length; i++) {
  tabsTitle[i].addEventListener('click', function () {

    const parentUl = this.closest(".tab-head");
    for (let i = 0; i < parentUl.children.length; i++) {
      parentUl.children[i].classList.remove("active");
    }

    this.classList.add("active");

    const tabsContent = document.querySelectorAll(".presents_box-description .tab-item");
    for (let i = 0; i < tabsContent.length; i++) {
      tabsContent[i].classList.remove("active");
    }

    const tabId = this.dataset.target;

    const tab = document.getElementById(tabId);
    tab.classList.add("active");
  });
}



// Style Option Select

$('.select').each(function () {
  const _this = $(this),
    selectOption = _this.find('option'),
    selectOptionLength = selectOption.length,
    selectedOption = selectOption.filter(':selected'),
    duration = 350; // длительность анимации 

  _this.hide();
  _this.wrap('<div class="select"></div>');
  $('<div>', {
    class: 'new-select',
    text: _this.children('option:disabled').text()
  }).insertAfter(_this);

  const selectHead = _this.next('.new-select');
  $('<div>', {
    class: 'new-select__list'
  }).insertAfter(selectHead);

  const selectList = selectHead.next('.new-select__list');
  for (let i = 1; i < selectOptionLength; i++) {
    $('<div>', {
      class: 'new-select__item',
      html: $('<span>', {
        text: selectOption.eq(i).text()
      })
    })
      .attr('data-value', selectOption.eq(i).val())
      .appendTo(selectList);
  }

  const selectItem = selectList.find('.new-select__item');
  selectList.slideUp(0);
  selectHead.on('click', function () {
    if (!$(this).hasClass('on')) {
      $(this).addClass('on');
      selectList.slideDown(duration);

      selectItem.on('click', function () {
        let chooseItem = $(this).data('value');

        $('select').val(chooseItem).attr('selected', 'selected');
        selectHead.text($(this).find('span').text());

        selectList.slideUp(duration);
        selectHead.removeClass('on');
      });

    } else {
      $(this).removeClass('on');
      selectList.slideUp(duration);
    }
  });
});

